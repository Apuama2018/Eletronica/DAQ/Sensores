// Sample Arduino MAX6675 Arduino Sketch

#include <max6675.h> //biblioteca do módulo
#include <SPI.h> //protocolo que funciona como um barramento, conectando o microcontrolador e outros componentes
#include <SD.h>

int ktcSO = 5;
int ktcCS = 6;
int ktcCLK = 7;
const int chipSelect = 7;
File dataFile;



MAX6675 ktc(ktcCLK, ktcCS, ktcSO);

  
void setup() {
  Serial.begin(9600); //tempo para o max6675 estabilizar
  delay(500);

  Serial.print("Initializing SD card...");

  if (!SD.begin(chipSelect)) { // checa se o cartão está presente e pode ser inicilizado
    Serial.println("Card failed, or not present");
    return;
  }
  Serial.println("card initialized.");
}

void loop() {
  
  dataFile = SD.open("datalog.csv", FILE_WRITE); //abre o arquivo no modo de leitura
  
   Serial.print("Deg C = "); // mostra na tela a leitura da temperatura
   Serial.print(ktc.readCelsius()); //faz a leitura da temperatura
   Serial.print("\t Deg F = ");
   Serial.println(ktc.readFahrenheit());

   if (dataFile) { //se o arquivo estiver aberto, grava no cartão SD
    dataFile.println(ktc.readCelsius());
    dataFile.println(ktc.readFahrenheit());
    dataFile.close(); //fecha o arquivo
    Serial.println("done.");
  } else {
    // mensagem de erro caso o arquivo não tenha aberto
    Serial.println("error opening test.txt");
  }
 
   delay(500);
}

