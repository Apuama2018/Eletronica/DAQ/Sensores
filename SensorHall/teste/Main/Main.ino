 const int HALLPin = 2;

int rpm = 0;
int time = 0;
int oldtime = 0;
float revolution = 0;
//const int LEDPin = 8;

void setup() {
  //pinMode(LEDPin, OUTPUT);
  //pinMode(HALLPin, INPUT); // To see if it is working properly
  attachInterrupt(0,isr,RISING);  //attaching the interrupt
  Serial.begin(9600);
}
 
void loop() {
  /*if(digitalRead(HALLPin)==LOW) //Sensor hall detected the gear tooth
  {
    Serial.println("Detected");
    //digitalWrite(LEDPin, HIGH);  
  }
  else
  {
    Serial.println("Undetected");
    //digitalWrite(LEDPin, LOW);
  }//To see if it is working properly */
  
  delay(1000);
  
  detachInterrupt(0);            // Stop to count
  time = millis() - oldtime;     //calculates the relative time 
  rpm = (revolution/time)*60000;        //calculates rpm
  oldtime = millis();            //saves the current time
  revolution = 0;
  
  Serial.print("RPM: ");
  Serial.println(rpm);
  
  attachInterrupt(0,isr,RISING);   
}

void isr(){ // Counts the pulses
  revolution++;
} 
