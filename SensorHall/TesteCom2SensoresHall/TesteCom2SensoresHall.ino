 //Declarando variáveis
volatile int rev = 0; //Rotação
//volatile int rev1 = 0; //Rotação
int voltas = 0;
//int voltas1 = 0;
unsigned int i = 0;  //Contador de loops
unsigned long lastmillis = 0;

void setup(){ //Setup
  Serial.begin(9600); //Inicia a comunicação serial 
  attachInterrupt(0, rpm, FALLING); //Inicia a contagem da rotação 
  //attachInterrupt(1, rpm1, FALLING); //Inicia a contagem da rotação 
}


void loop(){//Loop
  i++; //Incrementa o contador de número de loops
  Serial.println(rev);
  if (millis() - lastmillis >= 250){ //Atualiza a cada 250ms
    detachInterrupt(0); //Para a contagem da rotação
    //detachInterrupt(1); //Para a contagem da rotação
  
    Serial.print("1: ");
    Serial.println(rev);
    //Serial.print("  2: ");
    //Serial.println(rev1);
    
    lastmillis = millis(); // Update lasmillis
    attachInterrupt(0, rpm, FALLING); //Resume a contagem da rotação
    //attachInterrupt(1, rpm1, FALLING); //Resume a contagem da rotação
    if (rev >= 40) {
      voltas++;
      rev = 0;
    }
    //if (rev1 >= 40) {
     // voltas1++;
      //rev1 = 0;
    //}
   // Serial.println(voltas);
   // Serial.println(voltas);
  } 

}


void rpm(){//RPM
 rev++; //Incrementa o contador de pulsos
}
//void rpm1(){//RPM
// rev1++; //Incrementa o contador de pulsos
//}
