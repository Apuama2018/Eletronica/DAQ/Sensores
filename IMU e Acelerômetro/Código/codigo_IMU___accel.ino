

//definições do MMA 7455(próprio site do arduino)
//definições de entrada e sinal/latência
#define MMA7455_XOUTL 0x00      // Read only, Output Value X LSB
#define MMA7455_XOUTH 0x01      // Read only, Output Value X MSB
#define MMA7455_YOUTL 0x02      // Read only, Output Value Y LSB
#define MMA7455_YOUTH 0x03      // Read only, Output Value Y MSB
#define MMA7455_ZOUTL 0x04      // Read only, Output Value Z LSB
#define MMA7455_ZOUTH 0x05      // Read only, Output Value Z MSB
#define MMA7455_XOUT8 0x06      // Read only, Output Value X 8 bits
#define MMA7455_YOUT8 0x07      // Read only, Output Value Y 8 bits
#define MMA7455_ZOUT8 0x08      // Read only, Output Value Z 8 bits
#define MMA7455_STATUS 0x09     // Read only, Status Register
#define MMA7455_DETSRC 0x0A     // Read only, Detection Source Register
#define MMA7455_TOUT 0x0B       // Temperature Output Value (Optional)
#define MMA7455_RESERVED1 0x0C  // Reserved
#define MMA7455_I2CAD 0x0D      // Read/Write, I2C Device Address
#define MMA7455_USRINF 0x0E     // Read only, User Information (Optional)
#define MMA7455_WHOAMI 0x0F     // Read only, "Who am I" value (Optional)
#define MMA7455_XOFFL 0x10      // Read/Write, Offset Drift X LSB
#define MMA7455_XOFFH 0x11      // Read/Write, Offset Drift X MSB
#define MMA7455_YOFFL 0x12      // Read/Write, Offset Drift Y LSB
#define MMA7455_YOFFH 0x13      // Read/Write, Offset Drift Y MSB
#define MMA7455_ZOFFL 0x14      // Read/Write, Offset Drift Z LSB
#define MMA7455_ZOFFH 0x15      // Read/Write, Offset Drift Z MSB
#define MMA7455_MCTL 0x16       // Read/Write, Mode Control Register 
#define MMA7455_INTRST 0x17     // Read/Write, Interrupt Latch Reset
#define MMA7455_CTL1 0x18       // Read/Write, Control 1 Register
#define MMA7455_CTL2 0x19       // Read/Write, Control 2 Register
#define MMA7455_LDTH 0x1A       // Read/Write, Level Detection Threshold Limit Value
#define MMA7455_PDTH 0x1B       // Read/Write, Pulse Detection Threshold Limit Value
#define MMA7455_PD 0x1C         // Read/Write, Pulse Duration Value
#define MMA7455_LT 0x1D         // Read/Write, Latency Time Value (between pulses)
#define MMA7455_TW 0x1E         // Read/Write, Time Window for Second Pulse Value
#define MMA7455_RESERVED2 0x1F  // Reserved

#define MMA7455_D0 0
#define MMA7455_D1 1
#define MMA7455_D2 2
#define MMA7455_D3 3
#define MMA7455_D4 4
#define MMA7455_D5 5
#define MMA7455_D6 6
#define MMA7455_D7 7

// Status Register
#define MMA7455_DRDY MMA7455_D0
#define MMA7455_DOVR MMA7455_D1
#define MMA7455_PERR MMA7455_D2

// Mode Control Register
#define MMA7455_MODE0 MMA7455_D0
#define MMA7455_MODE1 MMA7455_D1
#define MMA7455_GLVL0 MMA7455_D2
#define MMA7455_GLVL1 MMA7455_D3
#define MMA7455_STON MMA7455_D4
#define MMA7455_SPI3W MMA7455_D5
#define MMA7455_DRPD MMA7455_D6

// Detection Source Register
#define MMA7455_INT1 MMA7455_D0
#define MMA7455_INT2 MMA7455_D1
#define MMA7455_PDZ MMA7455_D2
#define MMA7455_PDY MMA7455_D3
#define MMA7455_PDX MMA7455_D4
#define MMA7455_LDZ MMA7455_D5
#define MMA7455_LDY MMA7455_D6
#define MMA7455_LDX MMA7455_D7

//Biblioteca básica Arduino IDE
#include <SPI.h>

//Bibliotecas Cartão de memória básica no Arduino IDE
//Utilizando exemplo ReadWrite de domínio público
#include "SD.h" 

File myDados;
char arquivo_saida[30] = "Dados.txt";

//Bibliotecas relógio (https://github.com/adafruit/RTClib )
//Em domínio Público
#include "RTClib.h" 

// I2C Device Address Register
#define MMA7455_I2CDIS MMA7455_D7

//biblioteca do protocolo I2C 
#include "I2Cdev.h"
#include <Wire.h>

//biblioteca do MPU 6050
#include "MPU6050.h"

// Default I2C address for the MMA7455
#define MMA7455_I2C_ADDRESS 0x1D

//Variáveis do MPU 6050
MPU6050 accelgyro;
int16_t ax, ay, az;
int16_t gx, gy, gz;

//Definiçãpo para o código do MPU6050
#define OUTPUT_READABLE_ACCELGYRO

//Iniciando o relógio
RTC_Millis rtc;

//Variáveis para o funcionamento do MMA7455
typedef union xyz_union
{
  struct
  {
    uint8_t x_lsb;
    uint8_t x_msb;
    uint8_t y_lsb;
    uint8_t y_msb;
    uint8_t z_lsb;
    uint8_t z_msb;
  } reg;
  struct
  {
    uint16_t x;
    uint16_t y;
    uint16_t z;
  } value;
};

  //Variavel para contar o tempo
  double t = millis();
  
void setup()
{
   Serial.print("Iniciando SD card...");
    pinMode(10, OUTPUT);
    if (!SD.begin(4)) {
    Serial.println("SD Card conexão falha");
    return; }
    Serial.println("SD Card conecão bem sucedida");
    pinMode(10, OUTPUT);

    myDados = SD.open(arquivo_saida, FILE_WRITE);
    myDados.print("\tApuama - UnB");
    myDados.println("");
    myDados.print("\tDados do IMU (x,y,z) e do Acelerômetro (x,y,z), respectivamente");
    myDados.println("");
    myDados.println("");
    myDados.print("t"); myDados.print(",                         \t");
    myDados.print("Ax1"); myDados.print("\t");
    myDados.print("Ay1"); myDados.print("\t");
    myDados.print("Az1"); myDados.print(",\t");
    myDados.print("Ax2"); myDados.print("\t");
    myDados.print("Ay2"); myDados.print("\t");
    myDados.print("Az2"); myDados.print("\t");
    myDados.println("");

    myDados.close();
  
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    // inicialização da porta serial 
    // 38400 para a melhor funcionabilidade da leitura, indicado pelo datasheet
    Serial.begin(38400);
    Serial.print("\tApuama - UnB");
    Serial.println("");
    //Fazendo a leitura da hora inicial do sistema
    rtc.begin(DateTime(__DATE__, __TIME__));
    //Imprimindo os valores de data e hora
    DateTime now = rtc.now();

    Serial.println("");
    Serial.print(now.day(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.year(), DEC);
    Serial.println(' ');
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.println(now.second(), DEC);
    Serial.println("");

    Serial.print("\tDados do IMU e do Acelerômetro");
    Serial.println("");
    Serial.println("");


    
    // inicializando os dispositivos I2C
    Serial.println("Inicializando os dispositivos I2C...");
    accelgyro.initialize();

    // verificando conexão 
    Serial.println("Testando conexão do MPU6050");
    Serial.println(accelgyro.testConnection() ? "MPU6050 conexão bem sucedida" : "MPU6050 conexão falha");

    
  //Variáveis para checar erros no MMA7455
  int error;
  uint8_t c;

  //Inicialando o MM7455
  error = MMA7455_init();
  if (error == 0)
    Serial.println("MMA7455 conexão bem sucedida\n");
  else
    Serial.println("Não está funcionando");

  
    Serial.print("t"); Serial.print(",                         \t");
    Serial.print("Ax1"); Serial.print("\t");
    Serial.print("Ay1"); Serial.print("\t");
    Serial.print("Az1"); Serial.print(",\t");
    Serial.print("Ax2"); Serial.print("\t");
    Serial.print("Ay2"); Serial.print("\t");
    Serial.print("Az2"); Serial.print("\t");
    Serial.println("");
    Serial.println("");

}


void loop()
{   
  uint16_t x, y, z, error;
  double dX, dY, dZ;
  double t;

  // ler os valores brutos do acel/giro do dispositivo juntos
    //accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
    
    //Pegando dados brutos APENAS do giroscópio
    //accelgyro.getRotation(&gx, &gy, &gz);
    
    //Pegando dados brutos APENAS da aceleração
    accelgyro.getAcceleration(&ax, &ay, &az);

         
   
  // The function MMA7455_xyz returns the 'g'-force
  // as an integer in 64 per 'g'.

  // set x,y,z to zero (they are not written in case of an error).
  x = y = z = 0;
  error = MMA7455_xyz(&x, &y, &z); // get the accelerometer values.

  dX = (int16_t) x / 64.0;         // calculate the 'g' values.
  dY = (int16_t) y / 64.0;
  dZ = (int16_t) z / 64.0;

  t = millis();
    
        // Dados do acel/giro para acompanhar em tempo real e ou dê problema no SD
        Serial.print(t); Serial.print(",                     \t");
        Serial.print(ax/1716.61); Serial.print("\t");
        Serial.print(ay/1716.61); Serial.print("\t");
        Serial.print(az/1716.61); Serial.print(",\t");
        //Descomentar para obter os valores do giro no serial
        //Serial.print(gx); Serial.print("\t");
        //Serial.print(gy); Serial.print("\t");
        //Serial.print(gz); Serial.print("\t");
        
        Serial.print(dX*9.81,3);Serial.print("\t");
        Serial.print(dY*9.81,3);Serial.print("\t");
        Serial.println(dZ*9.81,3);
       
        
        myDados = SD.open(arquivo_saida, FILE_WRITE);
        // Dados do acel/giro para acompanhar em tempo real e ou dê problema no SD
        myDados.print(t); myDados.print(",                     \t");
        myDados.print(ax/1716.61); myDados.print("\t");
        myDados.print(ay/1716.61); myDados.print("\t");
        myDados.print(az/1716.61); myDados.print(",\t");
        //Descomentar para obter os valores do giro no serial
        //myDados.print(gx); Serial.print("\t");
        //myDados.print(gy); Serial.print("\t");
        //myDados.print(gz); Serial.print(",\t");
        
        myDados.print(dX*9.81,3);myDados.print("\t");
        myDados.print(dY*9.81,3);myDados.print("\t");
        myDados.println(dZ*9.81,3);
        delay(100);
        myDados.close();
       

}
//Função para calibrar o MMA7455
int MMA7455_init(void)
{
  uint16_t x, y, z;
  int error;
  xyz_union xyz;
  uint8_t c1, c2;

  // SENSIBILIDADE:
  //    2g : GLVL0
  //    4g : GLVL1
  //    8g : GLVL1 | GLVL0
  // Modo:
  //    Standby         : 0
  //    Measurement     : MODE0
  //    Level Detection : MODE1
  //    Pulse Detection : MODE1 | MODE0
 
  // Coloque a sensibilidade e o modo 
  c1 = bit(MMA7455_GLVL1) | bit(MMA7455_MODE0);
  error = MMA7455_write(MMA7455_MCTL, &c1, 1);
  if (error != 0)
    return (error);

  // Testa comunicação 
  error = MMA7455_read(MMA7455_MCTL, &c2, 1);
  if (error != 0)
    return (error);

  if (c1 != c2)
    return (-99);

  //Calibrando com os regitradores internos do sensor 
  xyz.value.x = xyz.value.y = xyz.value.z = 0;
  error = MMA7455_write(MMA7455_XOFFL, (uint8_t *) &xyz, 6);
  if (error != 0)
    return (error);
    
  delay(100);

#define USE_INTERNAL_OFFSET_REGISTERS
#ifdef USE_INTERNAL_OFFSET_REGISTERS

  //Contas para calibração do sensor do MMA7455

  error = MMA7455_xyz (&x, &y, &z); // get the x,y,z values
  if (error != 0)
    return (error);

  xyz.value.x = 2 * -x;        // The sensor wants double values.
  xyz.value.y = 2 * -y;
  xyz.value.z = 2 * -(z - 64); // 64 is for 1 'g' for z-axis.

  error = MMA7455_write(MMA7455_XOFFL, (uint8_t *) &xyz, 6);
  if (error != 0)
    return (error);

  // The offset has been set, and everything should be okay.
  // But by setting the offset, the offset of the sensor
  // changes.
  // A second offset calculation has to be done after
  // a short delay, to compensate for that.
  delay(200);

  error = MMA7455_xyz (&x, &y, &z);    // get te x,y,z values again
  if (error != 0)
    return (error);

  xyz.value.x += 2 * -x;       // add to previous value
  xyz.value.y += 2 * -y;
  xyz.value.z += 2 * -(z - 64); // 64 is for 1 'g' for z-axis.

  // Write the offset for a second time.
  // This time the offset is fine tuned.
  error = MMA7455_write(MMA7455_XOFFL, (uint8_t *) &xyz, 6);
  if (error != 0)
    return (error);

#endif

  return (0);          // return : no error
}



// MMA7455_xyz
//
// Get the 'g' forces.
// The values are with integers as 64 per 'g'.

//Função principal de funcionamento do MMA7455
int MMA7455_xyz( uint16_t *pX, uint16_t *pY, uint16_t *pZ)
{
  xyz_union xyz;
  int error;
  uint8_t c;

  // Wait for status bit DRDY to indicate that
  // all 3 axis are valid.
  do
  {
    error = MMA7455_read (MMA7455_STATUS, &c, 1);
  } while ( !bitRead(c, MMA7455_DRDY) && error == 0);
  if (error != 0)
    return (error);

  // Read 6 bytes, containing the X,Y,Z information
  // as 10-bit signed integers.
  error = MMA7455_read (MMA7455_XOUTL, (uint8_t *) &xyz, 6);
  if (error != 0)
    return (error);

  // The output is 10-bits and could be negative.
  // To use the output as a 16-bit signed integer,
  // the sign bit (bit 9) is extended for the 16 bits.
  if (xyz.reg.x_msb & 0x02)    // Bit 9 is sign bit.
    xyz.reg.x_msb |= 0xFC;     // Stretch bit 9 over other bits.
  else
    xyz.reg.x_msb &= 0x3;

  if (xyz.reg.y_msb & 0x02)
    xyz.reg.y_msb |= 0xFC;
  else
    xyz.reg.y_msb &= 0x3;

  if (xyz.reg.z_msb & 0x02)
    xyz.reg.z_msb |= 0xFC;
  else
    xyz.reg.z_msb &= 0x3;

  // The result is the g-force in units of 64 per 'g'.
  *pX = xyz.value.x;
  *pY = xyz.value.y;
  *pZ = xyz.value.z;

  return (0);                  // return : no error
}

// Daqui para baixo são funções de funcionamneto I2C do MMA7455 que a princípio não são relevantes mas essenciais para o sensor 
int MMA7455_read(int start, uint8_t *buffer, int size)
{
  int i, n, error;

  Wire.beginTransmission(MMA7455_I2C_ADDRESS);
  n = Wire.write(start);
  if (n != 1)
    return (-10);

  n = Wire.endTransmission(false); // hold the I2C-bus
  if (n != 0)
    return (n);

  // Third parameter is true: relase I2C-bus after data is read.
  Wire.requestFrom(MMA7455_I2C_ADDRESS, size, true);
  i = 0;
  while (Wire.available() && i < size)
  {
    buffer[i++] = Wire.read();
  }
  if ( i != size)
    return (-11);

  return (0);                  // return : no error
}



int MMA7455_write(int start, const uint8_t *pData, int size)
{
  int n, error;

  Wire.beginTransmission(MMA7455_I2C_ADDRESS);
  n = Wire.write(start);        // write the start address
  if (n != 1)
    return (-20);

  n = Wire.write(pData, size);  // write data bytes
  if (n != size)
    return (-21);

  error = Wire.endTransmission(true); // release the I2C-bus
  if (error != 0)
    return (error);

  return (0);                   // return : no error
  
}

