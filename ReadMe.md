# Sensor Hall

O princípio de funcionamento se baseia na variação do campo magnético produzido pela aproximação de um material ferromagnético, de modo a produzir uma variação de tensão.

- Pinagem: 

![Pinagem](/SensorHall/IMG-20180222-WA0025.jpg)

Onde o fio vermelho é 5 V, azul é o ground e o branco é o dado. Conectar na fueltech nos pinos 8 e 9 de dados (rodas dianteira e trazeira)



![Conexão](/SensorHall/pinout.png)

- Como testar o correto funcionamento: [teste do sensor Hall](https://www.youtube.com/watch?v=9DirjbUJKXE)

- *Alternativa viável de substituição do sensor hall:*

-> https://www.youtube.com/watch?v=iNBngx-atUg

-> https://www.youtube.com/watch?v=lN8ifTvhMvY


# Termopar

Os termopares são sensores de temperatura simples, mas eficientes e funcionam com base no Efeito Seeback. Nos termopares, esse efeito ocorre pela junção de dois metais, onde é gerada uma diferença de potencial causada pela diferença de temperatura entre os dois metaisO termopar utilizado nesse projeto é o tipo K e tem como finalidade realizar a medição da temperatura das pastilhas de freio do AF17. Além do próprio sensor, em conjunto é utilizado o módulo MAX-6675 e um Arduino.

Para a monategm, primeiro, o terminal positivo do fio do sensor, o vermelho, é conectado no terminal positivo do módulo MAX-6675 e o negativo, o azul, no terminal negativo do mesmo.
A pinagem do módulo é a seguinte:
-	SO: SPI - Serial Output, conectado em terminal digital do Arduino;
-	CS: SPI - Chip Select, também diretamente em terminal digital;
-	SCK: SPI - System Clock, conectado também em qualquer terminal digital do Arduino;
-	VCC: 5 Volts, conectado diretamente no terminal de 5 volts do Arduino;
-	GND: Terra, ligado no terra do Arduino.

# Sensor MAP

É o sensor responsável pela medição da pressão no coletor de admissão. O sensor funciona diretamente ligado a FT500 e a solução atual para conectar os seus pinos é utilizar um conector de áudio ("conector mosquitinho").
Pinagem:
- Pino 1: ground.
- Pino 2: NTC resistor.
- Pino 3: 5 V.
- Pino 4: Output Signal.

-> [tutorial](https://www.youtube.com/watch?v=lN8ifTvhMvY)
